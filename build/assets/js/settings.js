var config_module = angular.module('ixitLanding.config', []);
var config_data = {
  'appData': {
    'app_name': 'IXIT Landing',
    'filetypeIcons': [
      'accdb',
      'eml',
      'htm',
      'jsf',
      'no-img',
      'proj',
      'readme',
      'vsd',
      'xlsx_mac',
      'bmp',
      'eps',
      'ind',
      'midi',
      'pdf',
      'psd',
      'settings',
      'wav',
      'xlsx',
      'css',
      'fla',
      'ini',
      'mov',
      'png',
      'pst',
      'text',
      'wma',
      'doc',
      'gif',
      'jpeg',
      'mp3',
      'pptx_mac',
      'pub',
      'tiff',
      'wmv',
      'docx',
      'html',
      'jpg',
      'mpeg',
      'pptx_win',
      'rar',
      'url',
      'xls'
    ]
  },
  'template_dict': {
    'heroSectionPx': function () {
      var is_true = function (rc) {
        return rc === true;
      },
      condition = arguments[1] || '',
      return_value = {};

      var responsive_cfg = arguments[0] || {},
          pt_config = {
            'sm': '0em',
            'xs': '3em',
            'md': '0'
          },
          m_config = {
            'xs': '30px 0',
            'md' : '0 0 0 0'
          };
      return_value['padding-top'] = pt_config[_.findKey(responsive_cfg, is_true)];

      if (condition.length && condition === 'm') {
        return_value['margin'] = m_config[_.findKey(responsive_cfg, is_true)];
      }
      return return_value;
    },
    'heroSection': function () {
      var is_true = function (rc) {
        return rc === true;
      };
      var responsive_cfg = arguments[0] || {},
          config = {
            'sm': '0em',
            'xs': '6em',
            'md': '1em'
          };
      return {
        'padding-top': config[_.findKey(responsive_cfg, is_true)]
      };
    },
    'heroDivContainer': function () {
      var responsive_cfg = arguments[0] || {};
      return {
        'vmiddle': responsive_cfg.md,
        'sm-vmiddle': responsive_cfg.sm
      };
    }
  },
  'share_urls' : function (config) {
    return {
      'facebook': {
        'url' : function (f) {
          var u =
          //encodeURIComponent(
          'https://www.facebook.com/sharer/sharer.php?u=' +
          config.CONSUMER_API_URL +
          '/' +
          f.ixitId +
          '&t=iXit Messaging; Download available: ' +
          f.name;
          //);
          return u;
        },
        'title' : ''
      },
      'twitter' : {
        'url' : function (f) {
          return 'https://twitter.com/intent/tweet?text=' +
          '@ixitbot shared ' +
          f.name +
          ' .download here: ' +
          config.CONSUMER_API_URL +
          '/' +
          f.ixitId
          ;
        }
      }
    };

  },
  'api_config': {
    'PARSE_APP_KEY': 'cWLGlv32E6uTGhI914xKeZgYSFSi1feyPURmV6db',
    'PARSE_JS_KEY': 'lSSnUwO7iSvW0xC3owjo0Rbv5f7uoNJ4hgqp0HXO',
    'CONSUMER_API_URL': 'http://api.i-x.it',
    'FILEVAULT_API_URL': 'http://vault.i-x.it',
    // 'CONSUMER_API_URL': 'http://api.ixit.vm',
    // 'FILEVAULT_API_URL': 'http://vault.ixit.vm'

  }
};
angular.forEach(config_data,function(key,value) {
  config_module.constant(value,key);
});
