var app = angular.module('ixitLanding', [
  'flow',
  'ixitLanding.config',
  'ui.router'
  ]);


app.config([
  '$httpProvider',
  'flowFactoryProvider',
  'api_config',
  '$stateProvider',
  '$urlRouterProvider',
  function(
    $httpProvider,
    flowFactoryProvider,
    api_config,
    $stateProvider,
    $urlRouterProvider
    ) {

    $stateProvider
    .state('hello', {
      url: '/hello',
      views: {
        'hello' : {
          templateUrl: 'partials/hello.html'
        }
      }
    })
    .state('trial', {
      url: '/trial',
      views: {
        'hello': {
          templateUrl: 'partials/manage-upload.html',
          controller: 'appCtrl'
        }
      }
    });


    flowFactoryProvider.defaults = {
        target: api_config.FILEVAULT_API_URL + '/upload',
        chunkSize:1*1024*1024,
        simultaneousUploads:1,
        testChunks:true,
        permanentErrors:[400, 503, 500, 501, 502, 504, 505],
        maxChunkRetries: 10,
        chunkRetryInterval: 5000,
    };

    flowFactoryProvider.on('catchAll', function () {
      // console.log(arguments);
    });

  $httpProvider.interceptors.push(['$q', 'api_config', '$rootScope', function ($q, api_config, $rootScope) {
      return {
          'request': function (config) {
            $rootScope.$broadcast('app:is-requesting', true);
             config.headers['x-print-token'] = window.localStorage.userId;
             if (config.url.indexOf('/api/') > -1 ) {
                config.url = api_config.CONSUMER_API_URL + '' + config.url;
                return config || $q.when(config);
              } else {
               return config || $q.when(config);
              }
          },
          'response': function (resp) {
              $rootScope.$broadcast('app:is-requesting', false);
              // appBootStrap.isRequesting = false;
               return resp || $q.when(resp);
          },
          // optional method
         'responseError': function(response) {
            // do something on error
            if (response.status === 403 || response.status === 401) {
              $rootScope.$broadcast('auth:auth-login-required');
            }
            $rootScope.$broadcast('app:is-requesting', false);
            return $q.reject(response);
          },
          // optional method
         'requestError': function(response) {
            // do something on error
            $rootScope.$broadcast('app:is-requesting', false);
            return $q.reject(response);
          }

      };
  }]);


  $httpProvider.defaults.useXDomain = true;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/trial');

  if (Parse) {
    Parse.initialize(api_config.PARSE_APP_KEY, api_config.PARSE_JS_KEY);
  }

}]);
app.controller('hostCtrl', [
  '$scope',
  'template_dict',
  'share_urls',
  'api_config',
  function (
    $scope,
    template_dict,
    share_urls,
    api_config
    ) {
  $scope.responsive_cfg = {
    'md': false,
    'sm': true
  };
  $scope.$tpd = template_dict;
  $scope.$$$sh = share_urls(api_config);

  $scope.modal_state=false;

  // Setup the form to watch for the submit event
  $scope.submit_user_form = function submit_user_form (e, form){
    e.preventDefault();

    //Clears any pre-existing modal content
    $scope.modal_content = null;

    // shows the progress loader
    $scope.modal_loader = true;

    // show the modal
    $('.login').modal('show');

    //sets the modal content
    $scope.modal_content = {
      title: 'Patience',
      subtitle: 'iXit is working on it.'
    };

    // Grab the elements from the form to make up
    // an object containing name, email and message
    var data = {
      name: form.name,
      email: form.email,
      message: form.message,
      formtype: form.formtype,
      phoneNumber: form.phoneNumber
    },
    modal_cfg = {
      'pre_launch_new_user_registeration': {
        title: 'Good, Welldone, Awesome!',
        subtitle: 'We are super excited to have you. Please check your email for a complimentary welcome message.'
      },
      'contactform': {
        title: 'Email Sent',
        subtitle: 'Our support team will respond to this inquiry immediately. Please expect an email message or phone call follow-up.',
      },
      'error': {
        title: 'Something did not go well.',
        subtitle: 'We could not complete the last operation. Embarassing to us, but we are committed to making sure we fix it. Please try again.'
      }
    };

    // checks and validations
    if (!form.formtype) {
      $scope.modal_content = modal_cfg['error'];
      return false;
    }

    // checks and validations
    if(!Parse) {
      return false;
    }

    // Run our Parse Cloud Code and
    // pass our 'data' object to it
    Parse.Cloud.run(form.formtype, data, {
      success: function(object) {
        $scope.modal_content = modal_cfg[form.formtype];
        $scope.modal_loader = false;
        $scope.$apply();
        jQuery(document).trigger('button.with-xhr.complete');
      },

      error: function(object, error) {
        console.log(error);
        $scope.modal_content = modal_cfg['error'];
        $scope.modal_loader = false;
        $scope.$apply();
        jQuery(document).trigger('button.with-xhr.complete');
      }
    });
  };
}]);
app.controller('formCtrl', ['$scope', '$timeout', function ($scope, $timeout) {

}]);
app.controller('appCtrl', [
  '$scope',
  'appService',
  'api_config',
  function ($scope, appService, api_config) {
    $scope.config = api_config;
  //Store the fingerprint in localStorage
  window.localStorage.userId = window.fingerprint.md5hash;

  $scope.$flow.on('fileAdded', function (file) {
    if (file.flowObj.files.length >= 6) {
      return false;
    }

  });
  $scope.$flow.on('filesSubmitted', function () {

    $scope.$flow.upload();
  });



  $scope.$flow.on('fileSuccess', function (file, message, chunk) {
    var m = JSON.parse(message);
    if (m && m.mediaNumber) {
      appService.returnId(m.mediaNumber)
      .then(function (id) {
        file.ixitId = id.data;
      }, function (err) {
        file.error = true;
      });

    } else {
      file.error = true;
    }
    // appService.returnId()
  });

  $scope.file_is_uploading = function (f) {
    //if the file is uploading, and no errors, and is
    //not complete
    if (!f.error && !f.isComplete()) {
      return true;
    } else {
      return false;
    }
  };
  $scope.file_is_waiting = function (f) {
    return false;
    //if the file is uploading, and no errors, and is
    //not complete
    if (!f.isUploading() && !f.error && !f.isComplete()) {
      return true;
    } else {
      return false;
    }
  };
  $scope.file_is_complete = function (f) {
    //if the file is complete, and no errors, and is
    //not uploading
    if (!f.error && f.isComplete()) {
      return true;
    } else {
      return false;
    }
  };
  $scope.file_has_error = function (f) {
    //if the file has , and no errors, and is
    //not uploading
    if (f.error && f.isComplete()) {
      return true;
    } else {
      return false;
    }
  };
  $scope.calc_percentage = function (f) {
    var i = Math.round(f.progress() * 100);
    return i;
  };

}]);
app.service('appService', ['$http', function ($http) {
  return {
    returnId: function returnId (mediaId) {
      return $http.get('/api/p/getId/' + mediaId);
    }
  };
}]);
app.filter('totalUploadSize', function () {
  return function (files) {
    return _.reduce(files, function (initValue, currentValue) {
      return currentValue.size + initValue;
    }, 0);
  };
});
app.filter('uploadCount', function () {
  return function (queue) {
    var l = _.filter(queue, function (q) {
      return !q.isComplete() && !q.error;
    });
    return l.length;
  };
});
app.filter('formatFileSize', function(){
  return function(bytes){
    if (typeof bytes !== 'number') {
      return '';
    }
    if (bytes >= 1000000000) {
      return (bytes / 1000000000).toFixed(2) + ' GB';
    }
    if (bytes >= 1000000) {
      return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
  };
});
app.filter('moment', function(){
  return function(time){
    var m = moment(time);
    return m.fromNow();
  };
});
app.filter('timeLeft', function(){
  return function(secs){
    if (!isFinite(secs)) {
      return 'a very long time';
    }
    var when = moment().add(secs, 'seconds');

    return moment().to(when);
  };
});
app.directive('knob', [function () {
  return {
    link: function (scope, ele, attr) {
      var circle = new ProgressBar.Circle('#' + attr.id, {
          color: '#ffffff',
          strokeWidth: 3,
          trailWidth: 1,
          trailColor: '#C19E0A',
          duration: 1500,
          text: {
              value: '0',
              className: 'knob_value'
          },
          step: function(state, bar) {
              bar.setText((bar.value() * 100).toFixed(0));
          }
      });

      scope.$flow.on('fileProgress', function () {
        circle.animate(scope.$flow.progress());
      });
    }
  };
}]);
app.directive('clipboard', [function () {
  return {
    link: function link (scope, ele, attr) {
      ZeroClipboard.config({
        autoActivate: true,
        bubbleEvents: false,
      });
      var client = new ZeroClipboard( $(ele) );

      client.on( 'ready', function(event) {
        // console.log( 'movie is loaded' );

        client.on( 'copy', function(event) {
          event.clipboardData.setData('text/plain', location.host + '/' + scope.clipboard.ixitId);
        } );
        client.on( 'mouseover', function(event) {
          event.stopPropagation();
        } );
        client.on( 'mouseenter', function(event) {
          event.stopPropagation();
        } );
      } );

      client.on( 'error', function(event) {
        console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
        ZeroClipboard.destroy();
      } );
    },
    scope: {
      clipboard: '='
    }
  };
}]);
app.directive('addthisbuttons', [function () {
  return {
    link: function () {
      var addthis = addthis || false;
      if (!addthis) {
        //use jquery to load remote script
        //http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-561b1789e4daaa1e
        $.getScript('http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-561b1789e4daaa1e', function () {});
      }
    }
  };
}]);
app.directive('readyOnComplete', [function () {
  return {
    link: function (scope, ele, attr) {
      var e;
      if (!attr.tag || !attr.tag.length) {

        e = $('li', ele);
      } else {
        e = $(attr.tag, ele);
      }
      if (!e.length) {
        e = $(ele);
      }

      scope.$watch('f.ixitId', function (n) {
        if (n && n.length) {
          e.show();
        } else {
          e.hide();
        }

      });
    },
    scope: {
      f: '=readyOnComplete'
    }
  };
}]);
app.directive('disableWithXhr', [function () {
  return {
    link: function (scope, ele) {
      $(document).on('click', '.disable-with-xhr', function (e) {
        jQuery(e.currentTarget).prop('disabled', 'disabled');
      });
      $(document).on('button.with-xhr.complete', function (e) {
        jQuery(e.currentTarget).removeProp('disabled');
      });
    }
  };
}]);
app.directive('overlayHeight', [function () {

  return {
    link: function (scope, ele) {
      $(window).on('resize', function () {
        _.throttle(function () {
          $(ele).height($(window).height());
          return;
        }, 500);
      });
      // scope.$watch()

    }
  };
}]);
app.directive('responsiveView', function() {
  return {
    link: function (scope) {
      function window_size () {
        var width = $(window).width();
        //if the screen is small
        if (width <= 599) {
          scope.responsive_tpl = 'partials/responsive-xs.html';
          scope.responsive_cfg.sm = false;
          scope.responsive_cfg.xs = true;
          scope.responsive_cfg.md = false;
        }
        else if (width <= 767) {
          scope.responsive_tpl = 'partials/responsive-sm.html';
          scope.responsive_cfg.xs = false;
          scope.responsive_cfg.sm = true;
          scope.responsive_cfg.md = false;
        }
        else {
          scope.responsive_tpl = 'partials/responsive-md.html';
          scope.responsive_cfg.xs = false;
          scope.responsive_cfg.sm = false;
          scope.responsive_cfg.md = true;
        }
      }
      $(window).on('resize', window_size);
      window_size();
    }
  };
});
app.directive('panorama', function(){
  return {
    restrict: 'EA',
    link: function (scope, element, attrs){
      $(element).owlCarousel({
        pagination: false
      });
      scope.$watch('$flow.files.length', function (n, o) {
        $(element).hide();
        $(element).data('owlCarousel').destroy();
        $(element).owlCarousel();
        $(element).show();
      });
    }
  };
});
app.directive('panoramaSection',function() {
  return {
    restrict: 'C',
    link: function (s, e, a) {
      var ww = $(window).width();
      function onResize () {
        var w = parseInt(a.spanpct) || 77;
        if (s.responsive_cfg.md || s.responsive_cfg.sm || ww >= 690) {
          w = 45;
        }
        var p = (ww * w) / 100;
        $(e).width(p);
      }
      $(window).on('resize', onResize);
      onResize();
    }
  }
});