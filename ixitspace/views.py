from django.shortcuts import render_to_response




ng_tags =  {
  'n_' : '{{',
  'g_': '}}'
}

def index(request):
  request.session['member_id'] = 'Anon'
  return render_to_response('index.html', ng_tags)

def partials(request,  name):
  return render_to_response('partials/' + name, ng_tags)
